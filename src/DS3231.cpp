/*
 * Aging Offset unimplemented
 * Forced temperature conversion unimplemented
 */ 

#include <iostream>
using namespace std;

#include "DS3231.h"
#include<bitset>
#include<sstream>
#include<iomanip>

//RTC REGISTER ADDRESSES
#define SECONDS_REG 				0X00
#define MINUTES_REG 				0X01
#define HOUR_REG 					0X02
#define DAY_REG						0X03
#define DATE_REG					0X04
#define MONTH_REG					0X05
#define YEAR_REG					0X06

//ALARM 1 REGISTER ADDRESSES
#define SECONDS_ALARM_1_REG			0X07
#define MINUTES_ALARM_1_REG			0X08
#define HOUR_ALARM_1_REG			0X09
#define DAY_DATE_ALARM_1_REG		0X0A

//ALARM 2 REGISTER ADDRESSES
#define MINUTES_ALARM_2_REG			0X0B
#define HOUR_ALARM_2_REG			0X0C
#define DAY_DATE_ALARM_2_REG		0X0D

//TEMPERATURE REGISTER ADDRESSES
#define TEMP_MSB_REG				0X11
#define TEMP_LSB_REG				0X12

//MISC REGISTER ADDRESSES
#define CONTROL_REG					0X0E
#define STATUS_REG					0X0F
#define AGING_OFFSET_REG			0X10

//ALARM BITS
#define A1M1	7
#define A1M2	7
#define A1M3	7
#define A1M4	7
#define A2M2	7
#define A2M3	7
#define A2M4	7
#define DYDT 	6

//CONTROL BITS
#define	A1IE 	0
#define	A2IE	1
#define INTCN	2
#define	RS1		3
#define	RS2		4
#define	CONV	5
#define	BBSQW	6
#define	EOSC	7

//STATUS BITS
#define	A1F		0
#define	A2F		1
#define	BSY		2
#define	EN32KHZ	3
#define	OSF		7

//SIGN BIT
#define	SIGN	7

/*
 * Debug method to print out binary out
 */
string display(uint8_t a) {
   stringstream ss;
   ss << setw(3) << (int)a << "(" << bitset<8>(a) << ")";
   return ss.str();
}

DS3231::DS3231(unsigned int busNum,unsigned int addr):
I2C(busNum,addr){}

DS3231::~DS3231() {
}




/*
 * @brief 	This method returns the temperature value from the DS3231
 * 			msb hold the value read from the DS3231 reg 0x11
 * 			lsb hold the value read from the DS3231 reg 0x12
 * 			As stated in datasheet, the upper nibble in reg 0x12(i.e lsb) holds the fractional part
 * 			So, we shift 6 bits to get the upper nibble from the byte.
 * 			As the value is stored in 2's complement, we check the msb for the sign.
 *			As the resolution is 0.25 degrees, we multiply the reg value to get the actual temperature
 * @return Value of temperature in degree celcius
 */
float DS3231::getTemperature(){
	uint8_t msb, lsb;
	float complement, temp;

	msb = readI2cByte(TEMP_MSB_REG);
	lsb = (readI2cByte(TEMP_LSB_REG) >> 6);//used to get the decimal value

	if ((msb & _BV(SIGN)) != 0) //gets the sign bit out
		complement = msb | ~(_BV(SIGN) - 1); // get 2's complement if negative
	else
		complement = msb; //use as is

	temp = (0.25 * lsb) + complement;
	return temp;
}

/*
 * @brief Get value of hours in the DS3231 time registers
 * @return value of hours.
 */ 
int DS3231::getHours(void){
	char hour;
	hour = readI2cByte(HOUR_REG);
	return bcdToDec(hour);
}

/*
 * @brief Get value of minutes in the DS3231 time registers
 * @return value of minutes.
 */ 
int DS3231::getMinutes(void){
	char min;
	min = readI2cByte(MINUTES_REG);
	return bcdToDec(min);
}

/*
 * @brief Get value of seconds in the DS3231 time registers
 * @return value of seconds.
 */ 
int DS3231::getSeconds(void){
	char sec;
	sec = readI2cByte(SECONDS_REG);
	return bcdToDec(sec);
}

/*
 * @brief Set the value of Hours in the DS3231 time registers
 * @param value of hours to be set
 */ 
void DS3231::setHours(char hour){
	
	char hourDec = decToBcd(hour);
	writeI2cByte(HOUR_REG, hourDec);
}

/*
 * @brief Set the value of Minutes in the DS3231 time registers
 * @param value of Minutes to be set
 */ 
void DS3231::setMinutes(char min){

	char minDec = decToBcd(min);
	writeI2cByte(MINUTES_REG, minDec);
}

/*
 * @brief Set the value of Seconds in the DS3231 time registers
 * @param value of Seconds to be set
 */ 
void DS3231::setSeconds(char sec){

	char secDec = decToBcd(sec);
	writeI2cByte(SECONDS_REG, secDec);
}

/*
 * @brief Get the time according to the CTime struct
 * @return Refer to struct Ctime for its members
 */ 
cTime DS3231::getCtime(void){
	cTime myTime;
	myTime.tm_sec 	= getSeconds();
	myTime.tm_min 	= getMinutes();
    myTime.tm_hour 	= getHours(); 
    myTime.tm_mday	= getDay();
	myTime.tm_mon	= getMonth();
	myTime.tm_year	= getYear();
	myTime.tm_wday	= getDay();
	
	return myTime;
}

/*
 * @brief Get Day value from DS3231 Day register
 * @return Value of day
 */ 
int DS3231::getDay(void){
	char day;
	day = readI2cByte(DAY_REG);
	return bcdToDec(day);
}

/*
 * @brief Converts day recieved from DS3231 to Day name
 * @return Day name.
 */
string DS3231::toDayName(int name){
 string ret;
 	switch(name){
		case 1: 
			ret = "Sunday";
			break;
		case 2: 
			ret = "Monday";
			break;
		case 3: 
			ret = "Tuesday";
			break;
		case 4: 
			ret = "Wednesday";
			break;
		case 5: 
			ret = "Thursday";
			break;
		case 6: 
			ret = "Friday";
			break;
		case 7: 
			ret = "Saturday";
			break;
	}
	return ret;
}

/*
 * @brief Get Date value from DS3231 Date register
 * @return Value of date
 */ 
int DS3231::getDate(void){
	char date;
	date = readI2cByte(DATE_REG);
	return bcdToDec(date);
}

/*
 * @brief Get Month value from DS3231 Month register
 * @return Value of month
 */ 
int DS3231::getMonth(void){
	char month;
	month = readI2cByte(MONTH_REG);
	return bcdToDec(month);
}

/*
 * @brief Converts month recieved from DS3231 to month name
 * @return Value of month
 */ 
string DS3231::toMonthName(int name){
 string ret;
 	switch(name){
		case 1: 
			ret = "January";
			break;
		case 2: 
			ret = "February";
			break;
		case 3: 
			ret = "March";
			break;
		case 4: 
			ret = "April";
			break;
		case 5: 
			ret = "May";
			break;
		case 6: 
			ret = "June";
			break;
		case 7: 
			ret = "July";
			break;
		case 8:
			ret = "August";
			break;
		case 9:
			ret = "September";
			break;
		case 10:
			ret = "October";
			break;
		case 11:
			ret = "November";
			break;
		case 12:
			ret = "December";
			break;	
	}
	return ret;
}

/*
 * Get Year value from DS3231 Year register
 * @return Value of year
 */
int DS3231::getYear(void){
	char year;
	year = readI2cByte(YEAR_REG);
	return (2000+bcdToDec(year));
}

/*
 * @brief Set the value of day in the DS3231 day registers
 * @param value of day to be set
 */ 
void DS3231::setDay(char day){

	char secDay = decToBcd(day);
	writeI2cByte(DAY_REG, secDay);
}

/*
 * @brief Set the value of date in the DS3231 date registers
 * @param value of date to be set
 */ 
void DS3231::setDate(char date){

	char secDate = decToBcd(date);
	writeI2cByte(DATE_REG, secDate);
}

/*
 * @brief Set the value of month in the DS3231 month registers
 * @param value of Seconds to be set
 */ 
void DS3231::setMonth(char month){

	char secMonth = decToBcd(month);
	writeI2cByte(MONTH_REG, secMonth);
}

/*
 * @brief Set the value of year in the DS3231 year registers
 * @param value of year to be set
 */ 
void DS3231::setYear(uint16_t year){

	char secYear = decToBcd(year-2000);
	writeI2cByte(YEAR_REG, secYear);
}

/*
 * @brief 	Set the alarm 1 in the DS3231
 * 			Two seperate functions are provided to keep things safe
 * 			This ensures that alarm 2 functions are not triggerd on alarm 1 by any possibilty 
 *
 * @param	sec Seconds of the Alarm
 * @param	min Minutes of the Alarm
 * @param	hrs Hours of the alarm
 * @param	daydt Day or Date to be set
 * @param 	alarmType Alarm type to be selected from AlarmRate_1_et
 */
void DS3231::setAlarm_1(char sec, char min, char hrs, char daydt, AlarmRate_1_et alarmType){
	 sec = decToBcd(sec);
	 min = decToBcd(min);
	 hrs = decToBcd(hrs);
	 daydt = decToBcd(daydt);
    
	switch(alarmType){	//TODO testing
		
		case ONE_SEC_ALARM:
			sec |= _BV(A1M1);
			min |= _BV(A1M2);
			hrs |= _BV(A1M3);
			daydt |= _BV(A1M4);
			break;
			
		case SEC_MATCH_ALARM:
			sec &= ~(_BV(A1M1));
			min |= _BV(A1M2);
			hrs |= _BV(A1M3);
			daydt |= _BV(A1M4);
			break;
			
		case MIN_SEC_MATCH_ALARM:
			sec &= ~(_BV(A1M1));
			min &= ~(_BV(A1M2));
			hrs |= _BV(A1M3);
			daydt |= _BV(A1M4);
			break;
			
		case HRS_MIN_SEC_MATCH_ALARM:
			sec &= ~(_BV(A1M1));
			min &= ~(_BV(A1M2));
			hrs &= ~(_BV(A1M3));
			daydt |= _BV(A1M4);
			break;
			
		case DATE_HRS_MIN_SEC_MATCH_ALARM:
			sec &= ~(_BV(A1M1));
			min &= ~(_BV(A1M2));
			hrs &= ~(_BV(A1M3));
			daydt &= ~(_BV(A1M4));
			daydt &= ~(_BV(DYDT));
			break;
			
		case DAY_HRS_MIN_SEC_MATCH_ALARM:
			sec &= ~(_BV(A1M1));
			min &= ~(_BV(A1M2));
			hrs &= ~(_BV(A1M3));
			daydt &= ~(_BV(A1M4));
			daydt |= _BV(DYDT);
			break;
	}
	writeI2cByte(SECONDS_ALARM_1_REG, sec);
	writeI2cByte(MINUTES_ALARM_1_REG, min);
	writeI2cByte(HOUR_ALARM_1_REG, hrs);
	writeI2cByte(DAY_DATE_ALARM_1_REG, daydt);
	
}

/*
 * @brief Get the alarm 1 settings stored in the DS3231
 * @return Returns values of type AlarmSettings_1_st(refer for members).
 */
AlarmSettings_1_st DS3231::getAlarm_1(void){
	AlarmSettings_1_st alarmSettings;
	uint8_t sec,min,hr,dt;
	int type = 0;
	sec 	= readI2cByte(SECONDS_ALARM_1_REG);
	min 	= readI2cByte(MINUTES_ALARM_1_REG);
	hr 		= readI2cByte(HOUR_ALARM_1_REG);
	dt 		= readI2cByte(DAY_DATE_ALARM_1_REG);
	
	alarmSettings.seconds 	= (int)bcdToDec(sec & ~(_BV(A1M1)));
	alarmSettings.minutes	= (int)bcdToDec(min & ~(_BV(A1M2)));
	alarmSettings.hours		= (int)bcdToDec(hr & ~(_BV(A1M3)));
	alarmSettings.dayDate	= (int)bcdToDec(dt & ~(_BV(A1M4)));
	
	type |= ((sec & (_BV(A1M1))) >> 7);
	type |= ((min & (_BV(A1M2))) >> 6);
	type |= ((hr & (_BV(A1M3))) >> 5);
	type |= ((dt & (_BV(A1M4))) >> 4);
	type |= ((dt & (_BV(DYDT))) >> 2);
	alarmSettings.alarmType = (AlarmRate_1_et)type;
	
	return alarmSettings;
}

/*
 * @brief 	Set the alarm 2 in the DS3231
 * 			Two seperate functions are provided to keep things safe
 * 			This ensures that alarm 1 functions are not triggerd on alarm 2 by any possibilty 
 *
 * @param	min Minutes of the Alarm
 * @param	hrs Hours of the alarm
 * @param	daydt Day or Date to be set
 * @param 	alarmType Alarm type to be selected from AlarmRate_2_et
 */
void DS3231::setAlarm_2(char min, char hrs, char daydt, AlarmRate_2_et alarmType){
	min = decToBcd(min);
	hrs = decToBcd(hrs);
	daydt = decToBcd(daydt);
	
	switch(alarmType){
		
		case ONE_MIN_ALARM:
			min |= 	_BV(A2M2);
			hrs |= 	_BV(A2M3);
			daydt |= _BV(A2M4);
			break;
			
		case MIN_MATCH_ALARM:
			min &= ~(_BV(A2M2));
			hrs |= _BV(A2M3);
			daydt |= _BV(A2M4);
			break;
			
		case HRS_MIN_MATCH_ALARM:
			min &= ~(_BV(A2M2));
			hrs &= ~(_BV(A2M3));
			daydt |= _BV(A2M4);
			break;
			
		case DATE_HRS_MIN_MATCH_ALARM:
			min &= ~(_BV(A2M2));
			hrs &= ~(_BV(A2M3));
			daydt &= ~(_BV(A2M4));
			daydt &= ~(_BV(DYDT));
			break;
			
		case DAY_HRS_MIN_MATCH_ALARM:
			min &= ~(_BV(A2M2));
			hrs &= ~(_BV(A2M3));
			daydt &= ~(_BV(A2M4));
			daydt |= _BV(DYDT);
			break;
	}
	writeI2cByte(MINUTES_ALARM_2_REG, min);
	writeI2cByte(HOUR_ALARM_2_REG, hrs);
	writeI2cByte(DAY_DATE_ALARM_2_REG, daydt);
}
	
/*
 * @brief Get the alarm 2 settings stored in the DS3231 
 * @return Returns values of type AlarmSettings_2_st(refer for members).
 */
AlarmSettings_2_st DS3231::getAlarm_2(void){
	AlarmSettings_2_st alarmSettings;
	uint8_t min,hr,dt;
	int type = 0;
	min 	= readI2cByte(MINUTES_ALARM_2_REG);
	hr 		= readI2cByte(HOUR_ALARM_2_REG);
	dt 		= readI2cByte(DAY_DATE_ALARM_2_REG);
	
	alarmSettings.minutes	= (int)bcdToDec(min & ~(_BV(A2M2)));
	alarmSettings.hours		= (int)bcdToDec(hr & ~(_BV(A2M3)));
	alarmSettings.dayDate	= (int)bcdToDec(dt & ~(_BV(A2M4)));
	
	type |= ((min & (_BV(A2M2))) >> 6);
	type |= ((hr & (_BV(A2M3))) >> 5);
	type |= ((dt & (_BV(A2M4))) >> 4);
	type |= ((dt & (_BV(DYDT))) >> 2);
	alarmSettings.alarmType = (AlarmRate_2_et)type;
	
	return alarmSettings;
}

/*
 * @brief 	Enables to stop/start oscillator in Battery operation.
 * 		  	Default on boot up, oscilator is enabled 
 * @param 	state True to start oscillator, else stop oscillator
 */ 
void DS3231::enableOsc(bool state){
	uint8_t readVal;
	
	readVal = readI2cByte(CONTROL_REG);
	
	if(state){
		readVal &= ~(_BV(EOSC));
	}
	else{
		readVal |= _BV(EOSC);
	}	
	writeI2cByte(CONTROL_REG, readVal);
}

/*
 * @brief 	enables battery backed square wave.
 * 			Default value on Boot is false(0)
 * @param 	state true enables square wave, false disables it
 */ 
void DS3231::enableBbsqw(bool state){
	uint8_t readVal;
	
	readVal = readI2cByte(CONTROL_REG);
	
	if(!state){
		readVal &= ~(_BV(BBSQW));
	}
	else{
		readVal |= _BV(BBSQW);
	}
	writeI2cByte(CONTROL_REG, readVal);
}

/*
 * @brief	Enables alarm 1 to assert the status flag A1F in the status regester.
 * @param 	state true enables the alarm, false disables the alarm
 */
void DS3231::enableAlarm_1_Interrupt(bool state){
	uint8_t readVal;
	
	readVal = readI2cByte(CONTROL_REG);
	
	if(state){
		readVal |=  _BV(A1IE);
		readVal |=  _BV(INTCN);//TODO check if correct
	}
	else{
		readVal &= ~(_BV(A1IE));
	}	
	
	writeI2cByte(CONTROL_REG, readVal);
}

/*
 * @brief	Enables alarm 2 to assert the status flag A2F in the status regester.
 * @param 	state true enables the alarm, false disables the alarm
 */
void DS3231::enableAlarm_2_Interrupt(bool state){
	uint8_t readVal;
	
	readVal = readI2cByte(CONTROL_REG);
	
	if(state){
		readVal |=  _BV(A2IE);
		readVal |=  _BV(INTCN);
	}
	else{
		readVal &= ~(_BV(A2IE));
	}	
	writeI2cByte(CONTROL_REG, readVal);
}

/*
 * @brief	Gets status whether interrupt is enabled for alarm 1
 * @return 	true if interrupt enabled, else false
 */
bool DS3231::isAlarm_1_Enabled(void){
	uint8_t readVal;
	
	readVal = readI2cByte(CONTROL_REG);
	if(readVal &= _BV(A1IE)) return true;
	else return false;
}

/*
 * @brief	Gets status whether interrupt is enabled for alarm 2	
 * @return 	true if interrupt enabled, else false
 */
bool DS3231::isAlarm_2_Enabled(void){
	uint8_t readVal;
	
	readVal = readI2cByte(CONTROL_REG);
	if(readVal &= _BV(A2IE)) return true;
	else return false;
}	

//~ void DS3231::convertTemperature(bool state){//TODO
	//~ uint8_t readVal;
	
	//~ readVal = readI2cByte(CONTROL_REG);
	
	//~ if(state){
		//~ readVal |=  _BV(CONV);
	//~ }
	//~ else{
		//~ readVal &= ~(_BV(CONV));
	//~ }	
//~ }

/*
 * @brief	Starts the Square wave functionality
 * @param	frequency Select frequency from SquareWave_et enum
 */
void DS3231::setSqWaveFreq(SquareWave_et frequency){
	
	uint8_t readVal;
	
	readVal = readI2cByte(CONTROL_REG);
	
	switch(frequency){
		case Freq_1_HZ:
			readVal &= ~(_BV(RS1));
			readVal &= ~(_BV(RS2));
			break;
			
		case Freq_1024_HZ:
			readVal |= _BV(RS1);
			readVal &= ~(_BV(RS2));
			break;
		
		case Freq_4096_HZ:
			readVal &= ~(_BV(RS1));
			readVal |= _BV(RS2);
			break;
			
		case Freq_8192_HZ:
			readVal |= _BV(RS1);
			readVal |= _BV(RS2);
			break;
	}
	readVal &= ~(_BV(INTCN));
	writeI2cByte(CONTROL_REG, readVal);
}

SquareWave_et DS3231::getSqWaveFreq(void){
	int freq = 0;
	uint8_t readVal = readI2cByte(CONTROL_REG);
	
	freq |= ((readVal & (_BV(RS1))) >> 3);
	freq |= ((readVal & (_BV(RS2))) >> 3);
}	

/*
 * @brief 	Returns the status of the OSF flag in the status register.
 * 			Note: On first boot, the OSF flag is set by default.
 * @return 	true if oscillator is stopped set, else false
 */
bool DS3231::isOscStopped(void){
	uint8_t readVal = readI2cByte(STATUS_REG);
	if(readVal &= _BV(OSF)) return true;
	
	return false;
}

/*
 * @brief 	Clears the OSF flag in the status register.
 */
void DS3231::clearOscStopped(void){
	uint8_t readVal = readI2cByte(STATUS_REG);
	
	readVal &= ~(_BV(OSF));
	writeI2cByte(STATUS_REG, readVal);
}	

/*
 * @brief 	Returns the status of EN32Khz flag in the status register.
 * 			Note: By default the EN32Khz flag is set to 1.
 * @return 	true if 32Khz is enabled, else false
 */
bool DS3231::isEn32Khz(void){
	uint8_t readVal = readI2cByte(STATUS_REG);
	if(readVal &= _BV(EN32KHZ)) return true;
	
	return false;
}

/*
 * @brief 	Sets EN32Khz flag in status register.
 * 			Enables the 32Khz functionality.
 */
void DS3231::enable32Khz(bool enable){
	uint8_t readVal = readI2cByte(STATUS_REG);
	
	if(enable){
		readVal |= _BV(EN32KHZ);
	}
	else {
		readVal &= ~(_BV(EN32KHZ));
	}
	writeI2cByte(STATUS_REG, readVal);
}

/*
 * @brief 	Returns the status of the BSY flag in the status register.
 * 			The status indicates the RS3231 is busy in TCXO functions
 * @return 	true if busy, else false
 */
bool DS3231::isBusy(void){
	uint8_t readVal = readI2cByte(STATUS_REG);
	if(readVal &= _BV(BSY)) return true;
	
	return false;
}

/*
 * @brief 	Returns whether alarm 1 has been triggered.
 * @return 	true if alarm 1 has been triggered, else false
 */
bool DS3231::isAlarm_1_Status(void){
	uint8_t readVal = readI2cByte(STATUS_REG);
	if(readVal &= _BV(A1F)) return true;
	
	return false;
}

/*
 * @brief 	Clears Alarm 1 triggered status.
 */
void DS3231::clearAlarm_1_Status(void){
	uint8_t readVal = readI2cByte(STATUS_REG);
	
	readVal &= ~(_BV(A1F));
	writeI2cByte(STATUS_REG, readVal);
}

/*
 * @brief 	Returns whether alarm 2 has been triggered.
 * @return 	true if alarm 2 has been triggered, else false
 */	
bool DS3231::isAlarm_2_Status(void){
	uint8_t readVal = readI2cByte(STATUS_REG);
	if(readVal &= _BV(A2F)) return true;
	
	return false;
}

/*
 * @brief 	Clears Alarm 2 triggered status.
 */
void DS3231::clearAlarm_2_Status(void){
	uint8_t readVal = readI2cByte(STATUS_REG);
	
	readVal &= ~(_BV(A2F));
	writeI2cByte(STATUS_REG, readVal);
}
