#ifndef I2C_H_
#define I2C_H_

#define BUS_0 "/dev/i2c-0"
#define BUS_1 "/dev/i2c-1"
#include <stdint.h>
/*
 * @class I2cHal
 * @brief Hardware abstraction i2c read and write methods 
 * 		  need to be changed to adapt/port to any microcontrollers
 */
 
class I2C{
private:
	int file;
	unsigned int bus;
	unsigned int addr;
	
public:
	I2C(unsigned int bus,unsigned int addr);
	virtual ~I2C();
	virtual bool openConnection();
	virtual void closeConnection();
	virtual bool writeI2cByte(unsigned char reg, unsigned char data);
	virtual uint8_t readI2cByte(unsigned char reg);
};

#endif
