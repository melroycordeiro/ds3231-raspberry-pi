#include "I2c.h"

#include <iostream>
#include <stdint.h>
using namespace std;

#include<sys/ioctl.h>
#include<linux/i2c.h>
#include<linux/i2c-dev.h>
#include<fcntl.h>
#include<unistd.h>

/*
 * @brief Constructor of the I2C class.
 * @param bus It is the bus number
 * @param addr Device i2c address
 */
I2C::I2C(unsigned int bus,unsigned int addr):
bus(bus),addr(addr){
	openConnection();
}

/*
 * @brief 	opens a connection with the i2c device
 * @return 	true if connection has opened, false if connection failed
 */ 
bool I2C:: openConnection(){
	string busName;
	if(bus == 0)busName = BUS_0;
	else busName = BUS_1;
	if((this->file=open(busName.c_str(), O_RDWR)) < 0){
	perror("failed to open the bus\n");
	return 0;
	}
	
	if(ioctl(this->file, I2C_SLAVE, this->addr) < 0){
	perror("Failed to connect to the sensor\n");
	return 0;
	}
	
	return 1;
}

/*
 * @brief 	Reads a byte from the specified register
 * @param 	reg Register address to be accessed.
 * @return 	Read byte from register.
 */
uint8_t I2C::readI2cByte(unsigned char reg){

	uint8_t buf[1];
	unsigned char writeBuffer[1] = {reg};

	if(write(this->file, writeBuffer, 1)!=1){
		perror("Failed to reset the read address\n");
	  //~ return -1;
	  /*
	   * ERROR 
	   * return 0 is wrong as the register can return 0, just done to break from the function
	   * either return a -1(cannot be done here due to uint8_t)
	   * changing to int8_t limits us to size of 128 but our reg values go upto 255
	   */
	  return 0; 
	}

	if(read(this->file, buf, 1)!=1){
		perror("Failed to read in the buffer\n");
	return 1;
	}
	return buf[0];
}

/*
 * @brief	Writes a byte to the specified register
 * @param 	reg Register address to be accessed.
 * @param 	data Data to be written in the register.
 * @return 	Returns false on failure, true on success.
 */
bool I2C::writeI2cByte(unsigned char reg, unsigned char data){ 

unsigned char writeBuffer[2];
	
	writeBuffer[0] = reg;
	writeBuffer[1] = data;

	if(write(this->file, writeBuffer, 2)!=2){
	   perror("Failed to write data\n");
	  return false;
	}
	return true;
}

/*
 * @brief Closes the connection
 */
void I2C::closeConnection(){
	::close(this->file);
}

/*
 * @brief Destructor ensures file is closed
 */
I2C::~I2C() {
	closeConnection();
}
