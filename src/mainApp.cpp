#include <iostream>
#include "DS3231.h"

using namespace std;

#define DS3231_ADDR 0x68

int main() {

	DS3231 myRtc(1,DS3231_ADDR);
	cTime timeStruct;
	AlarmSettings_1_st alarmSettings_1;
	AlarmSettings_2_st alarmSettings_2;
	
	//Setting time 
	myRtc.setSeconds(50);
	myRtc.setMinutes(10);
	myRtc.setHours(12);
	//Setting date & day 
	myRtc.setDay(FRIDAY);
	myRtc.setDate(20);
	myRtc.setMonth(SEPTEMBER);
	myRtc.setYear(2019);

	
	//displaying temperature, time, date and day.
	cout << "Temperature is: " << myRtc.getTemperature() << " degree celcius" << endl;	
	cout << "Time is " << myRtc.getHours() << ":" << myRtc.getMinutes() << ":" << myRtc.getSeconds() << endl;
	cout << "Today is " << myRtc.getDate() << "/" << myRtc.getMonth() << "/" << myRtc.getYear() << endl;
	cout << "Today is " << myRtc.getDate() << "/" << myRtc.toMonthName(myRtc.getMonth()) << "/" << myRtc.getYear() << endl;
	cout << "The day is: " << myRtc.toDayName(myRtc.getDay()) <<endl;
	
	//uisng the cTime struct
	timeStruct = myRtc.getCtime();
	cout << "Ctime: " << endl;
	cout << timeStruct.tm_sec << endl;
	cout << timeStruct.tm_min << endl;
	cout << timeStruct.tm_hour << endl;
	cout << timeStruct.tm_mday << endl;
	cout << timeStruct.tm_mon << endl;
	cout << timeStruct.tm_year << endl;
	cout << timeStruct.tm_wday << endl;
	
	/*
	 * If the user needs to modify just one setting in the alarm reg
	 * eg. Only alarmType has to be changed
	 * The user has to first getAlarm and read the previous settings
	 * and use those to set the new alarm while changing the alarmType
	 * NOTE: enableAlarm_1_Interrupt/enableAlarm_2_Interrupt has to be used to enable interrupts for each alarm
	 * NOTE: After each alarm interrupt, clearAlarm_1_Status/clearAlarm_2_Status need to be called to clear the interrupt.
	 */
	 //Setting alarms
	myRtc.setAlarm_1(1,1,1,1,DAY_HRS_MIN_SEC_MATCH_ALARM);
	alarmSettings_1 = myRtc.getAlarm_1();
	cout << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << endl;
	cout << "Alarm 1: " << endl;
	cout << alarmSettings_1.seconds << endl;
	cout << alarmSettings_1.minutes << endl;
	cout << alarmSettings_1.hours << endl;
	cout << alarmSettings_1.dayDate << endl;
	cout << alarmSettings_1.alarmType << endl;
	cout << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << endl;
	
	myRtc.setAlarm_2(10,12,MONDAY, ONE_MIN_ALARM);
	alarmSettings_2 = myRtc.getAlarm_2();
	cout << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << endl;
	cout << "Alarm 2: " << endl;
	cout << alarmSettings_2.minutes << endl;
	cout << alarmSettings_2.hours << endl;
	cout << alarmSettings_2.dayDate << endl;
	cout << alarmSettings_2.alarmType << endl;
	cout << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << endl;
	
	myRtc.enableAlarm_1_Interrupt(true);
	if(myRtc.isAlarm_1_Enabled())cout <<"Alarm 1 is enabled" <<endl;
	
	myRtc.enableAlarm_2_Interrupt(true);
	if(myRtc.isAlarm_2_Enabled())cout <<"Alarm 2 is enabled" <<endl;
	
	if(myRtc.isAlarm_1_Status()){
		cout << "Alarm 1 triggered" << endl;
		myRtc.clearAlarm_1_Status();
		if(!myRtc.isAlarm_1_Status()){
		  cout << "Alarm 1 cleared" << endl;
		}
	}
	
	if(myRtc.isAlarm_2_Status()){
		cout << "Alarm 2 triggered" << endl;
		 myRtc.clearAlarm_2_Status();
		if(!myRtc.isAlarm_2_Status()){
		  cout << "Alarm 2 cleared" << endl;
		}
	}
	
	//setting squarewave output
	myRtc.setSqWaveFreq(Freq_1024_HZ);
	myRtc.getSqWaveFreq();

	//reading oscillator status and clearing it
	if(myRtc.isOscStopped()){
	cout << "Oscillator Stopped" << endl;
	myRtc.clearOscStopped();
		if(!myRtc.isOscStopped()){
		cout << "Oscillator Flag Clear" << endl;
		}
	}
	
	//enabling and disabling 32Khz output
	if(myRtc.isEn32Khz()){
	cout << "32 Khz Enabled" << endl;
	myRtc.enable32Khz(false);
		if(!myRtc.isEn32Khz()){
		cout << "32 Khz Disabled" << endl;
		}
	myRtc.enable32Khz(true);
		if(myRtc.isEn32Khz()){
		cout << "32 Khz Enabled" << endl;
		}		
	}
	
	//checking for Busy status of the DS3231
	if(myRtc.isBusy()){
		cout << "Rtc Busy" << endl;
	}
	
	//~ //Enabling battery backed square wave functionality
	myRtc.enableBbsqw(true);
	
	
		
}
