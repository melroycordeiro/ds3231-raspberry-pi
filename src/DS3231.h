#ifndef DS3231_H_
#define DS3231_H_

#include "I2c.h"

enum AlarmRate_1_et{
	ONE_SEC_ALARM = 				0x0F,
	SEC_MATCH_ALARM = 				0x0E,
	MIN_SEC_MATCH_ALARM = 			0x0C,
	HRS_MIN_SEC_MATCH_ALARM = 		0x08,
	DATE_HRS_MIN_SEC_MATCH_ALARM = 	0x00,
	DAY_HRS_MIN_SEC_MATCH_ALARM	=	0x10
};

enum AlarmRate_2_et{
	ONE_MIN_ALARM = 				0x0E,
	MIN_MATCH_ALARM = 				0x0C,
	HRS_MIN_MATCH_ALARM = 			0x08,
	DATE_HRS_MIN_MATCH_ALARM = 		0x00,
	DAY_HRS_MIN_MATCH_ALARM	=		0x10
};

enum SquareWave_et{
	Freq_1_HZ = 	0,
	Freq_1024_HZ = 	1,
	Freq_4096_HZ = 	2,
	Freq_8192_HZ = 	3
};

enum Day_et{
	SUNDAY 		= 1,
	MONDAY		= 2,
	TUESDAY		= 3,
	WEDNESDAY	= 4,
	THURSDAY	= 5,
	FRIDAY		= 6,
	SATURDAY	= 7
};	

enum Month_et{
	JANUARY		= 1,
	FEBRUARY	= 2,
	MARCH		= 3,
	APRIL		= 4,
	MAY			= 5,
	JUNE		= 6,
	JULY		= 7,
	AUGUST		= 8,
	SEPTEMBER	= 9,
	OCTOBER		= 10,
	NOVEMBER	= 11,
	DECEMBER	= 12	
};

struct cTime {
   int tm_sec;    /* Seconds (0-60) */
   int tm_min;    /* Minutes (0-59) */
   int tm_hour;   /* Hours (0-23) */
   int tm_mday;   /* Day of the month (1-31) */
   int tm_mon;    /* Month (1-12) */
   int tm_year;   /* Year - 1900 */
   int tm_wday;   /* Day of the week (1-7, Sunday = 1) */
};

struct AlarmSettings_1_st{
	int seconds = 0;
	int minutes	= 0;
	int hours	= 0;
	int dayDate	= 0;
	AlarmRate_1_et alarmType;
};	

struct AlarmSettings_2_st{
	int minutes	= 0;
	int hours	= 0;
	int dayDate	= 0;
	AlarmRate_2_et alarmType;
};	

/*
 * @class Class I2C 
 * @brief Class for the DS3231 RTC. Provides functionality for time and date, alarms with interrupt,
 * 		  square wave with frequency selection , all of the status and control regester functions.
 * 		  Inhertits from hthe I2C class, protected makes sure the user does not get access to public methods of the I2C class.
 */ 
class DS3231:protected I2C {
public:

	DS3231(unsigned int busNum,unsigned int addr);
	virtual ~DS3231();

	virtual void setSeconds(char);
	virtual void setMinutes(char);
	virtual void setHours(char);

	virtual int getSeconds(void);
	virtual int getMinutes(void);
	virtual int getHours(void);
	
	virtual cTime getCtime(void);
	virtual void setDay(char);
	virtual void setDate(char);
	virtual void setMonth(char);
	virtual void setYear(uint16_t);

	virtual int getDay(void);
	virtual std::string toDayName(int name);
	virtual int getDate(void);
	virtual int getMonth(void);
	virtual std::string toMonthName(int name);
	virtual int getYear(void);

	virtual void setAlarm_1(char sec, char min, char hrs, char daydt, AlarmRate_1_et alarmType);
	virtual void setAlarm_2(char min, char hrs, char daydt, AlarmRate_2_et alarmType);
	virtual AlarmSettings_1_st getAlarm_1(void);
	virtual AlarmSettings_2_st getAlarm_2(void);
	virtual void enableAlarm_1_Interrupt(bool);
	virtual void enableAlarm_2_Interrupt(bool);
	virtual bool isAlarm_1_Enabled(void);
	virtual bool isAlarm_2_Enabled(void);

	virtual void enableOsc(bool);
	virtual void enableBbsqw(bool);
	//~ virtual void convertTemperature(bool);
	virtual void setSqWaveFreq(SquareWave_et);
	virtual SquareWave_et getSqWaveFreq(void);
	virtual float getTemperature(void);
	
	virtual bool isOscStopped(void);
	virtual void clearOscStopped(void);
	virtual bool isEn32Khz(void);
	virtual void enable32Khz(bool state);
	virtual bool isBusy(void);
	virtual bool isAlarm_1_Status(void);
	virtual void clearAlarm_1_Status(void);
	virtual bool isAlarm_2_Status(void);
	virtual void clearAlarm_2_Status(void);

private:
	inline int bcdToDec(char b) { return (b/16)*10 + (b%16); }
	inline int decToBcd(char b) { return (b/10)*16 + (b%10); }
};


#ifndef _BV
#define _BV(bit) (1 << (bit))
#endif

#endif /* DS3231_H_ */

